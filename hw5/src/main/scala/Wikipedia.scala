import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object Wikipedia {

val SPARK_MASTER = "spark://ec2-54-172-75-234.compute-1.amazonaws.com:7077"
val HDFS = "hdfs://ec2-54-172-75-234.compute-1.amazonaws.com:9000"

    val linksPath = HDFS + "/links-simple-sorted.txt"
    val titlesPath = HDFS + "/titles-sorted.txt"
    def main(args: Array[String]) {
        val conf = new SparkConf()
            .setMaster(SPARK_MASTER)
            .setAppName("Wikipedia")

        val sc = new SparkContext(conf)
        val sqlContext = new org.apache.spark.sql.SQLContext(sc)
	import org.apache.spark.sql._
	import org.apache.spark.sql.types._
	import sqlContext.implicits._ 
	
	val logData = sc.textFile(linksPath, 100)
        val titlesData = sc.textFile(titlesPath, 100)

	val schemaString = "mypages outs"
	val schema = StructType(schemaString.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
	val rowRDDoutlinks = logData
	.map(_.split(":")).filter(f => f!=null).map(p => Row(p(0).toString.trim,p(1).toString.trim))

	val indexList = sc.parallelize(List.range(1, titlesData.count().toInt))

	val schemaStringindex = "mypages"
	val schemaIndex = StructType(schemaStringindex.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
	val rowRDDindex = indexList.map(p=>Row(p.toString.trim))

	val dfOutlinks = sqlContext.createDataFrame(rowRDDoutlinks, schema)
	val dfIndex = sqlContext.createDataFrame(rowRDDindex, schemaIndex)
	// Register the DataFrames as a table.
	dfOutlinks.registerTempTable("outlinks")
	dfIndex.registerTempTable("indices")
	

	val outlinksTable = sqlContext.sql("SELECT indices.mypages FROM indices LEFT OUTER JOIN outlinks ON indices.mypages=outlinks.mypages WHERE outlinks.mypages IS NULL")

outlinksTable.collect().foreach(println)

outlinksTable.explain(true)

    }
    
}
