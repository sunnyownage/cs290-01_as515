import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import java.util.Calendar

object WhiteHouse {
val SPARK_MASTER = "spark://ec2-54-175-109-159.compute-1.amazonaws.com:7077"
val HDFS = "hdfs://ec2-54-175-109-159.compute-1.amazonaws.com:9000"
    val inputFilePath = HDFS + "/White_House_Visitor_Records_Requests.csv"
    def main(args: Array[String]) {
        val conf = new SparkConf()
            .setMaster(SPARK_MASTER)
            .setAppName("WhiteHouse")

        val sc = new SparkContext(conf)
        val sqlContext = new org.apache.spark.sql.SQLContext(sc)
	import org.apache.spark.sql._
	import org.apache.spark.sql.types._
	import sqlContext.implicits._ 
        val logData = sc.textFile(inputFilePath)

	// The schema of the data is encoded in a string
	val schemaString = "NAMELAST NAMEFIRST NAMEMID UIN BDGNBR ACCESS_TYPE TOA POA TOD POD APPT_MADE_DATE APPT_START_DATE APPT_END_DATE APPT_CANCEL_DATE Total_People LAST_UPDATEDBY POST LastEntryDate TERMINAL_SUFFIX visitee_namelast visitee_namefirst MEETING_LOC MEETING_ROOM CALLER_NAME_LAST CALLER_NAME_FIRST CALLER_ROOM Description Release_Date"

	// Generate the schema based on the string of schema
	val schema = StructType(schemaString.split(" ").map(fieldName => StructField(fieldName, StringType, true)))

//val rowRDDvisitors = data.map(_.split(",")).map(p => Row(p(0).trim,p(1).trim,p(2).trim))
//val rowRDDvisitees = data.map(_.split(",")).map(p => Row(p(19).trim,p(20).trim))

val rowRDDcombination = logData
	.map(_.split(",")).map(p => Row(p(0).trim,p(1).trim,p(2).trim,p(3).trim,p(4).trim,p(5).trim,p(6).trim,p(7).trim,p(8).trim,p(9).trim,p(10).trim,p(11).trim,p(12).trim,p(13).trim,p(14).trim,p(15).trim,p(16).trim,p(17).trim,p(18).trim,p(19).trim,p(20).trim))

// Apply the schema to the RDD to create a DataFrame
//val dfvisitors = sqlContext.createDataFrame(rowRDDvisitors, schema)
//val dfvisitees = sqlContext.createDataFrame(rowRDDvisitees, schema)
val dfcombination = sqlContext.createDataFrame(rowRDDcombination, schema)

// Register the DataFrames as a table.
//dfvisitors.registerTempTable("visitors")
//dfvisitees.registerTempTable("visitees")
dfcombination.registerTempTable("combination")

// SQL statements can be run by using the sql methods provided by sqlContext
val visitorsTable = sqlContext.sql("SELECT NAMELAST,NAMEFIRST,NAMEMID,COUNT(*) AS cnt FROM combination GROUP BY NAMELAST,NAMEFIRST,NAMEMID ORDER BY cnt DESC LIMIT 10")
val visiteesTable = sqlContext.sql("SELECT visitee_namelast,visitee_namefirst,COUNT(*) AS cnt FROM combination GROUP BY visitee_namelast,visitee_namefirst ORDER BY cnt DESC LIMIT 10")
val combinationTable = sqlContext.sql("SELECT NAMELAST,NAMEFIRST,NAMEMID,visitee_namelast,visitee_namefirst,COUNT(*) AS cnt FROM combination GROUP BY NAMELAST,NAMEFIRST,NAMEMID,visitee_namelast,visitee_namefirst ORDER BY cnt DESC LIMIT 10")
// The results of SQL queries are DataFrames and support all the 
// normal RDD operations.
// The columns of a row in the result can be accessed by ordinal value
val dataVisitors = visitorsTable.map(t => t(0).toString+","+t(1).toString+","+t(2).toString+",Count:"+t(3).toString).collect().foreach(println)
val dataVisitees = visiteesTable.map(t => t(0).toString+","+t(1).toString+",Count:"+t(2).toString).collect().foreach(println)
val dataCombination = combinationTable.map(t => t(0).toString+","+t(1).toString+","+t(2).toString+"->"+t(3).toString+","+t(4).toString+",Count:"+t(5).toString).collect().foreach(println)

// See the plans using explain(true)
visitorsTable.explain(true)
visiteesTable.explain(true)
combinationTable.explain(true)
        
    }
}

