import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import collection.JavaConversions._
object WikipediaInLink {
val SPARK_MASTER = "spark://ec2-54-172-75-234.compute-1.amazonaws.com:7077"
val HDFS = "hdfs://ec2-54-172-75-234.compute-1.amazonaws.com:9000"

    val linksPath = HDFS + "/links-simple-sorted.txt"
    val titlesPath = HDFS + "/titles-sorted.txt"
    def main(args: Array[String]) {
        val conf = new SparkConf()
            .setMaster(SPARK_MASTER)
            .setAppName("WikipediaInLink")

        val sc = new SparkContext(conf)
        val sqlContext = new org.apache.spark.sql.SQLContext(sc)
	import org.apache.spark.sql._
	import org.apache.spark.sql.types._
	import sqlContext.implicits._ 
        
	// Creates textfile RDD with 100 partitions each
	val logData = sc.textFile(linksPath, 100)

        val titlesData = sc.textFile(titlesPath, 100)

	// I perform a split after the ":", then split the result by spaces, and make is a flatmap to perform my next transformations. Then I filter out the blanks.
        val inLinks = logData.map(line=>line.split(":")(1))
            .flatMap(f => f.split(" "))
            .filter(a => !a.equals(""))

	// I take the distinct inlinks, as if they are not distinct, there definitely are inlinks
      //  val distinctLinks = inLinks.distinct()

	val schemaString = "links"

	// Generate the schema based on the string of schema
	val schema = StructType(schemaString.split(" ").map(fieldName => StructField(fieldName, StringType, true)))

	val dfinlinks = sqlContext.createDataFrame(inLinks.map(l => Row(l)), schema)

	dfinlinks.registerTempTable("inlinks")

	val indexList = sc.parallelize(List.range(1, titlesData.count().toInt))

	val schemaStringindex = "mypages"
	val schemaIndex = StructType(schemaStringindex.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
	val rowRDDindex = indexList.map(p=>Row(p.toString.trim))
	
	val dfIndex = sqlContext.createDataFrame(rowRDDindex, schemaIndex)
	dfIndex.registerTempTable("indices")

	val inlinksTable = sqlContext.sql("SELECT indices.mypages FROM indices LEFT OUTER JOIN inlinks ON indices.mypages=inlinks.links WHERE inlinks.links IS NULL")

	inlinksTable.collect().foreach(println)

	inlinksTable.explain(true)
       
    }
    
}

