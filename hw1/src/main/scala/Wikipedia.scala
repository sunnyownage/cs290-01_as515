import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object Wikipedia {

val SPARK_MASTER = "spark://ec2-54-174-246-11.compute-1.amazonaws.com:7077"
val HDFS = "hdfs://ec2-54-174-246-11.compute-1.amazonaws.com:9000"

    val linksPath = HDFS + "/links-simple-sorted.txt"
    val titlesPath = HDFS + "/titles-sorted.txt"
    def main(args: Array[String]) {
        val conf = new SparkConf()
            .setMaster(SPARK_MASTER)
            .setAppName("Wikipedia")

        val sc = new SparkContext(conf)
	
	val logData = sc.textFile(linksPath)        

        val titlesData = sc.textFile(titlesPath)

        val outLinks = logData.map(line=>line.split(":")(0))

        val outLinkNums = outLinks.map(f=>f.toInt)

        val titlesMap = titlesData.zipWithIndex()
            .map(w => (w._2.toInt, w._1))

        val indexList = sc.parallelize(List.range(1, titlesData.count().toInt))

        val results = indexList.subtract(outLinkNums).collect()

        val finalResult = titlesMap.filter(f=>results.contains(f._1))

        finalResult.collect().foreach(a=>println(a._2))
       
    }
    
}

