import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import collection.JavaConversions._
object WikipediaInLink {
val SPARK_MASTER = "spark://ec2-52-91-154-174.compute-1.amazonaws.com:7077"
val HDFS = "hdfs://ec2-52-91-154-174.compute-1.amazonaws.com:9000"

    val linksPath = HDFS + "/links-simple-sorted.txt"
    val titlesPath = HDFS + "/titles-sorted.txt"
    def main(args: Array[String]) {
        val conf = new SparkConf()
            .setMaster(SPARK_MASTER)
            .setAppName("WikipediaInLink")

        val sc = new SparkContext(conf)
        
	val logData = sc.textFile(linksPath, 500)

        val titlesData = sc.textFile(titlesPath, 500)

        val inLinks = logData.map(line=>line.split(":")(1))
            .flatMap(f => f.split(" "))
            .filter(a => !a.equals(""))

        val distinctLinks = inLinks.distinct()

        val inLinkNums = distinctLinks.map(f=>f.toInt)

        val titlesMap = titlesData.zipWithIndex()
            .map(w=>(w._2.toInt+1, w._1))

        val indexList = sc.parallelize(List.range(0, titlesData.count().toInt))

        val results = indexList.subtract(inLinkNums).collect()

        val finalResult = titlesMap.filter(f=>results.contains(f._1))

        finalResult.foreach(a=>println(a._2))
       
    }
    
}

