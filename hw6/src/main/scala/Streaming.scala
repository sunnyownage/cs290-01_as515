import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark._
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._


object Streaming {

    def main(args: Array[String]) {
        val conf = new SparkConf()
            .setMaster("local[*]")
            .setAppName("Streaming")

	// Create the streaming context, make current directory the checkpoint folder, and create our file stream
        val ssc = new StreamingContext(conf, Seconds(1))
	ssc.checkpoint(".")
	//val lines = ssc.textFileStream("/tmp/spark-events")
	
	val lines = KafkaUtils.createStream(ssc, "localhost:2181", "test-consumer-group", "test")

	//producer setup
	val props = new HashMap[String, Object]()
	    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
	    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
	      "org.apache.kafka.common.serialization.StringSerializer")
	    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
	      "org.apache.kafka.common.serialization.StringSerializer")

	val producer = new KafkaProducer[String, String](props)

	producer.send("/tmp/spark-events/*")


	// Filters and maps for all transformations, counts, and averages
	val applicationstartcount = lines.filter(s => s.contains("SparkListenerApplicationStart"))
	val appmap = applicationstartcount.map(x => ("Number of Applications", 1))
	val linesmap = lines.filter(s => s.contains("Event")).map(line => line.split(":")(1)).map(e => (e, 1))
	val taskstartmap = lines.filter(s => s.contains("SparkListenerTaskStart"))
	val taskstartid = taskstartmap.map(line => line.split(",")(3)).map(e => (e.split(":")(2), 1))
	val taskendmap = lines.filter(s => s.contains("SparkListenerTaskEnd"))
	val taskendid = taskendmap.map(line => line.split(",")(5)).map(e => e.split(":")(2))
	
	// Incomplete task for start is 1, for end is -1
	val taskstartcount = taskstartmap.map(e => ("Incomplete Tasks", 1))
	val taskendcount = taskendmap.map(e => ("Incomplete Tasks", -1))

	// Grabs total time of all tasks.
	val tasktotaltime = taskendmap.map(line => ("Total Time", (line.split(",")(14).split(":")(1).toLong - line.split(",")(8).split(":")(1).toLong).toInt))

	// Grabs time of each individual task. Necessary for counting and doing averages.
	val taskindividualtime = taskendmap.map(line => (line.split(",")(5), (line.split(",")(14).split(":")(1).toLong - line.split(",")(8).split(":")(1).toLong).toInt))
	
	// Performs the update function so that our counts/values persist.
	val updateFunc = (values: Seq[Int], state: Option[Int]) => {
		val currentCount = values.foldLeft(0)(_+_)
		val previousCount = state.getOrElse(0)
		Some(currentCount + previousCount)
	}

	// Updates application starts (Part 1)
	val appcounts = appmap.updateStateByKey(updateFunc)

	// Updates total number of unique events (Part 2)
	val events = linesmap.updateStateByKey(updateFunc)
	val uniqueevents = events.count()

	// Finds number of tasks that have started but not completed (Part 3)
	val starttaskcounts = taskstartcount.updateStateByKey(updateFunc)
	val endtaskcounts = taskendcount.updateStateByKey(updateFunc)
	val totalcount = starttaskcounts.union(endtaskcounts).reduceByKey((x, y) => x+y)

	// Finds total average running time (Part 4)
	val individualtasktimes = taskindividualtime.updateStateByKey(updateFunc)
	

	// Parts 4 and 5
	val totaltaskcount = individualtasktimes.foreachRDD(rdd => {
		val count = rdd.count()
		if (count > 0){
			val values = rdd.values.map(x => (1, x))
			val totaltime = values.reduceByKey((x, y) => x+y)
			val result = totaltime.map(x => x._2/count).collect().head
			val greaterthanavg = rdd.filter(x => x._2 > result)
			println("\n\n\n\n ------ \n\n\n Average task time: " + result.toString + "\n\n\n")
			println("Tasks with runtime greater than average: ")
			greaterthanavg.collect().foreach(println)
			println("\n\n\n")
		}
	})

	// Part 1
	//appcounts.print

	// Part 2
	//uniqueevents.print

	// Part 3
	//totalcount.print

	ssc.start()
    	ssc.awaitTermination()


	}
}
