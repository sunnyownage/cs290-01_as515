import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import java.util.Calendar

object WhiteHouse {
val SPARK_MASTER = "spark://ec2-54-174-246-11.compute-1.amazonaws.com:7077"
val HDFS = "hdfs://ec2-54-174-246-11.compute-1.amazonaws.com:9000"
    val inputFilePath = HDFS + "/White_House_Visitor_Records_Requests.csv"
    def main(args: Array[String]) {
        val conf = new SparkConf()
            .setMaster(SPARK_MASTER)
            .setAppName("WhiteHouse")

        val sc = new SparkContext(conf)
        
        val logData = sc.textFile(inputFilePath)

        val dataVisitors = logData
            .map(line => (line.split(",")(0)
            .trim() + "," + line.split(",")(1)
            .trim() + "," + line.split(",")(2)).trim())

        val dataVisitees = logData.map(line => (line.split(",")(19)
                .trim() + "," + line.split(",")(20).trim()))

        val dataCombination = logData
                .map(line => (line.split(",")(0).trim() + ","
                        + line.split(",")(1).trim() + ","
                        + line.split(",")(2).trim() + " -> "
                        + line.split(",")(19).trim() + "," + line.split(",")(20)
                        .trim()))
        sortAndOutput(dataVisitors)
        sortAndOutput(dataVisitees)
        sortAndOutput(dataCombination)
        
    }

    def sortAndOutput(rdd : RDD[String]){
        val visitorCount = rdd
                .filter(f => !f.equals(","))
                .map(w => (w, 1))
                .reduceByKey((x, y) => x + y)

        val mostFrequent = visitorCount
            .takeOrdered(10)(Ordering[Int].reverse.on(x=>x._2))
	//val time = Calendar.getInstance().getTime()
        mostFrequent.foreach(println)
    }
    
}

